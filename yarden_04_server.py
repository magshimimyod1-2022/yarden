import socket

LISTEN_PORT = 1104


def main():
    my_dict = {1: "album list",
               2: "songs in album",
               3: "length of song",
               4: "song lyric",
               5: "song location in album",
               6: "search song by name",
               7: "search song by lyric"}
    while True:
        # Create a listening socket
        listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('', LISTEN_PORT)
        listening_sock.bind(server_address)
        listening_sock.listen(1)

        # Create a new conversation socket
        client_soc, client_address = listening_sock.accept()

        hello_msg = "Welcome!"
        client_soc.sendall(hello_msg.encode())

        # getting info from client
        client_msg = client_soc.recv(1024)
        client_msg = client_msg.decode()

        option = int(client_msg)
        if option == 8:
            msg = "quit"
            client_soc.sendall(msg.encode())
            exit()
        elif option in my_dict.keys():
            msg = my_dict[option]
            client_soc.sendall(msg.encode())
        else:
            msg = "option not existing."
            client_soc.sendall(msg.encode())

        client_soc.close()

if __name__ == "__main__":
    main()
