import socket

SERVER_PORT = 1104
SERVER_IP = "127.0.0.1"


def main():
    while True:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        # getting response from server
        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()

        print(server_msg + """\n
        1: "album list"
        2: "songs in album"
        3: "length of song"
        4: "song lyric"
        5: "song location in album"
        6: "search song by name"
        7: "search song by lyric"
        8: "quit"
        """)
        while True:
            try:
                msg = input("Please enter option: ")
                test = int(msg)
                break
            except ValueError:
                print("The input was not a valid integer.")

        sock.sendall(msg.encode())
        # getting response from server
        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        if "quit" in server_msg:
            exit()
        print(server_msg)


if __name__ == "__main__":
    main()
